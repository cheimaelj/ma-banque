//
//  Ma_BanqueTests.swift
//  Ma BanqueTests
//
//  Created by celj on 24/04/2023.
//

import XCTest
@testable import Ma_Banque

class AppCoordinatorTests: XCTestCase {
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testWindowIsKey() {
        let window = WindowStub()
        let coordinator = AppCoordinator(window: window)
        coordinator.start()
        XCTAssertTrue(window.makeKeyAndVisibleCalled)
        XCTAssertNotNil(window.rootViewController)
    }
}

private class WindowStub: UIWindow {
    var makeKeyAndVisibleCalled = false
    override func makeKeyAndVisible() {
        makeKeyAndVisibleCalled = true
    }
}
