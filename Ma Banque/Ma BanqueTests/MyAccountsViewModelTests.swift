//
//  MyAccountsViewModelTests.swift
//  Ma BanqueTests
//
//  Created by celj on 28/04/2023.
//

import XCTest
@testable import Ma_Banque

class MyAccountsViewModelTests: XCTestCase {

    var myAccountsViewModel = MyAccountsViewModel()
    
    override func setUp() {
        super.setUp()
        
        myAccountsViewModel.banks = getBanksMock()
        myAccountsViewModel.filterBanks(with: getBanksMock())
    }
    
    func testGetBanks(){
        myAccountsViewModel.banks = getBanksMock()
        myAccountsViewModel.filterBanks(with: getBanksMock())
        let bank1 = myAccountsViewModel.getCABanks(at: 0)
        let bank2 = myAccountsViewModel.getCABanks(at: 10)
        let bank3 = myAccountsViewModel.getOtherBanks(at: -5)

        XCTAssert(bank1 != nil )
        XCTAssert(bank1?.name == "Bank1")
        
        XCTAssert(bank2 == nil )
        XCTAssert(bank3 == nil )
    }
    
    
    func testRefreshData(){
        myAccountsViewModel.refeshBanks(with: getBanksMock())
        XCTAssert(myAccountsViewModel.banks.count == getBanksMock().count)
    }
    
}

extension MyAccountsViewModelTests {
    
    func getBanksMock() -> [Bank] {

        let bank1 = Bank(name: "Bank1", isCA: 1, accounts: [
            Account(order: 1, id: "idAccount", holder: "holder", role: 1, contract_number: "123456", label: "label", product_code: "123", balance: 123, operations: [
                Operation(id: "idOperation", title: "titleOperation", amount: "123", category: "category", date: "1644611558"),
                Operation(id: "idOperation", title: "titleOperation", amount: "123", category: "category", date: "1644621558"),
            ]),
            Account(order: 2, id: "idAccount", holder: "holder", role: 1, contract_number: "123456", label: "label", product_code: "123", balance: 123, operations: [
                Operation(id: "idOperation", title: "titleOperation", amount: "123", category: "category", date: "1644611558"),
                Operation(id: "idOperation", title: "titleOperation", amount: "123", category: "category", date: "1644621558"),
            ])
        ])
        
        let bank2 = Bank(name: "Bank2", isCA: 0, accounts: [
            Account(order: 1, id: "idAccount", holder: "holder", role: 1, contract_number: "123456", label: "label", product_code: "123", balance: 123, operations: [
                Operation(id: "idOperation", title: "titleOperation", amount: "123", category: "category", date: "1644611558"),
                Operation(id: "idOperation", title: "titleOperation", amount: "123", category: "category", date: "1644621558"),
            ]),
            Account(order: 2, id: "idAccount", holder: "holder", role: 1, contract_number: "123456", label: "label", product_code: "123", balance: 123, operations: [
                Operation(id: "idOperation", title: "titleOperation", amount: "123", category: "category", date: "1644611558"),
                Operation(id: "idOperation", title: "titleOperation", amount: "123", category: "category", date: "1644621558"),
            ])
        ])
        return [bank1, bank2]
    }
    
    
   
}
