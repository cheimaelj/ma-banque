//
//  TabBarCoordinator.swift
//  Ma Banque
//
//  Created by celj on 25/04/2023.
//

import Foundation
import UIKit

class TabBarCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    private var navigationController : UINavigationController
    var parentCoordinator: Coordinator?
    let tabBarController = UITabBarController()
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        guard let tabBarController = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "TabBarViewController") as? TabBarViewController else {
                return
        }
        let viewModel = TabBarViewModel()
        tabBarController.viewModel = viewModel
        tabBarController.viewModel?.coordinator = self
      
        navigationController.setViewControllers([tabBarController], animated: false)
        
    }
   
    
    func showMyAccountView() {
        let myAccountsCoordinator = MyAccountsCoordinator(navigationController: navigationController)
        myAccountsCoordinator.start()
        childCoordinators.append(myAccountsCoordinator)
    }
    
}
