//
//  MyAccountsTableViewCell.swift
//  Ma Banque
//
//  Created by celj on 27/04/2023.
//

import UIKit

class MyAccountsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tableView: UITableView!
    private var accounts: [Account] = []
    
    var showAccountDetails: ((Account)->Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.register(UINib(nibName: "MyAccountTableViewCell", bundle: nil), forCellReuseIdentifier: "MyAccountTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(with accounts: [Account]){
        self.accounts = accounts
        self.tableView.reloadData()
    }
    
}

extension MyAccountsTableViewCell: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accounts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MyAccountTableViewCell", for: indexPath) as? MyAccountTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(with: accounts[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.showAccountDetails?(accounts[indexPath.row])
    }
    
    
    
}
