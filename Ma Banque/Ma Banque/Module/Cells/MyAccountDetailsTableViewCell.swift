//
//  MyAccountDetailsTableViewCell.swift
//  Ma Banque
//
//  Created by celj on 27/04/2023.
//

import UIKit

class MyAccountDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(with operation: Operation){
        titleLabel.text = operation.title
        dateLabel.text = formatUnixTimestamp(operation.date)
        valueLabel.text = (operation.amount ?? "") +  " €"
    }
    
}
