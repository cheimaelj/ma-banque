//
//  MyAccountTableViewCell.swift
//  Ma Banque
//
//  Created by celj on 27/04/2023.
//

import UIKit

class MyAccountTableViewCell: UITableViewCell {
    
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(with account: Account){
        self.titleLabel.text = account.holder
        self.valueLabel.text = String(account.balance ?? 0) +  " €"
    }
    
}
