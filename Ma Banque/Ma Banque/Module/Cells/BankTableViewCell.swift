//
//  BankTableViewCell.swift
//  Ma Banque
//
//  Created by celj on 25/04/2023.
//

import UIKit

class BankTableViewCell: UITableViewCell {
    
    @IBOutlet weak var showAccountsImage: UIImageView!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(with bank: Bank, showAccounts: Bool){
        let totalBalances = bank.accounts?.reduce(0.0, { $0 + ($1.balance ?? 0) }) ?? 0.0

        valueLabel.text = String(format: "%.2f", totalBalances) + " €"
        titleLabel.text = bank.name
        if showAccounts {
            self.showAccountsImage.image = UIImage(systemName: "chevron.up")
        }else{
            self.showAccountsImage.image = UIImage(systemName: "chevron.down")
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}

