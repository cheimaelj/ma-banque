//
//  MyAccountDetailsViewController.swift
//  Ma Banque
//
//  Created by celj on 27/04/2023.
//

import UIKit

class MyAccountDetailsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    var viewModel: MyAccountDetailsViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        hidesBottomBarWhenPushed = false
    }
    
    func setUpView(){
        tableView.register(UINib(nibName: "MyAccountDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "MyAccountDetailsTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
        titleLabel.text = viewModel?.account?.label
        balanceLabel.text = String(viewModel?.account?.balance ?? 0) + " €"
        self.tableView.reloadData()
    }
    
    
}

extension MyAccountDetailsViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.account?.operations?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MyAccountDetailsTableViewCell", for: indexPath) as? MyAccountDetailsTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(with: viewModel?.account?.operations?[indexPath.row] ?? Operation(id: "", title: "", amount: "", category: "", date: ""))
        
        return cell
    }
    
}
