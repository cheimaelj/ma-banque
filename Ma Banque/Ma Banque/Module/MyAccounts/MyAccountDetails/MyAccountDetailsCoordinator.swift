//
//  MyAccountDetailsCoordinator.swift
//  Ma Banque
//
//  Created by celj on 27/04/2023.
//

import Foundation
import UIKit

class MyAccountDetailsCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var parentCoordinator: Coordinator?
    private let account: Account
    private var navigationController : UINavigationController
        
    init(navigationController: UINavigationController , account: Account) {
        self.navigationController = navigationController
        self.account = account
    }
    
    func start() {
        guard let viewController = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "MyAccountDetailsViewController") as? MyAccountDetailsViewController else {
                return
        }
        let viewModel = MyAccountDetailsViewModel(account: account)
        viewController.viewModel = viewModel
        viewController.viewModel?.coordinator = self
        viewController.hidesBottomBarWhenPushed = false
        self.navigationController.pushViewController(viewController, animated: true)
    }
    
    func myAccountDetailsDidFinish() {
        parentCoordinator?.childDidFinish(childCoordinator: self)
    }
}
