//
//  MyAccountDetailsViewModel.swift
//  Ma Banque
//
//  Created by celj on 27/04/2023.
//

import Foundation

class MyAccountDetailsViewModel{
    
    var account: Account?
    var coordinator: MyAccountDetailsCoordinator?
    
    init(account: Account) {
        self.account = account
        
        if let operations = account.operations {
            self.account?.operations = operations.sorted(by: {
                if let date1 = $0.date, let date2 = $1.date {
                    return date1 > date2 || (date1 == date2 && $0.title ?? "" < $1.title ?? "")
                }
                return false
            })
        }
        



    }
}
