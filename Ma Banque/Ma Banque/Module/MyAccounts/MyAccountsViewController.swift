//
//  MyAccountsViewController.swift
//  Ma Banque
//
//  Created by celj on 25/04/2023.
//

import UIKit

class MyAccountsViewController: UIViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: MyAccountsViewModel = MyAccountsViewModel()
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        setupViewModel()
        setUpTableView()
    }
    
    private func setupViewModel() {
        viewModel.showLoader = { [weak self] shouldShow in
            shouldShow ? self?.activityIndicator.startAnimating() : self?.activityIndicator.stopAnimating()
            self?.activityIndicator.isHidden =  !shouldShow
        }
        
        viewModel.refreshView = {
            self.tableView.reloadData()
        }
        
        viewModel.showError = { [weak self]  error in
             self?.showError(error: error)
        }
        
        viewModel.getBanks()
    }
    
    
    private func setUpView(){
        navigationItem.title = "mes_comptes".localized
        tabBarItem.title = "mes_comptes".localized
        navigationController?.navigationBar.prefersLargeTitles = true
        self.activityIndicator.isHidden = true
        
        self.viewModel.coordinator =  MyAccountsCoordinator(navigationController: self.navigationController!)
    }
    
    private func setUpTableView(){
        tableView.register(UINib(nibName: "BankTableViewCell", bundle: nil), forCellReuseIdentifier: "BankTableViewCell")
        tableView.register(UINib(nibName: "MyAccountsTableViewCell", bundle: nil), forCellReuseIdentifier: "MyAccountsTableViewCell")
        tableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
    }
    
    // This function is used to display an error message in an alert view.
    private func showError(error: String) {
        // Create an instance of UIAlertController with the error message as the title and an empty message.
        let alertController = UIAlertController(title: error, message: "", preferredStyle: .alert)
        
        // Create an instance of UIAlertAction with a "OK" title and default style.
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        // Add the UIAlertAction instance to the UIAlertController instance.
        alertController.addAction(action)
        
        // Present the UIAlertController instance to the user with an animation.
        self.present(alertController, animated: true, completion: nil)
    }
    
}

extension MyAccountsViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    // This function is used to provide the title for each section header in the table view.
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        switch section {
        case 0:
            // If the section number is 0, return the section title for the Credit Agricole .
            return AccountType.creditAgricole.sectionTitle
        case 1:
            // If the section number is 1, return the section title for the Other account .
            return AccountType.other.sectionTitle
        default:
            return nil
        }
    }
    
    
    // This function is used to determine the number of rows in each section of the table view.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == AccountType.creditAgricole.sectionIndex {
            // If the section number is the Credit Agricole section, return twice the number of CA banks to display both bank name and account number.
            return self.viewModel.cABanks.count * 2
        } else if section == AccountType.other.sectionIndex {
            // If the section number is the Other section, return twice the number of other banks to display both bank name and account number.
            return self.viewModel.otherBanks.count * 2
        }
        return 0
    }

    
    // This function is called when the user selects a row in the table view.
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == AccountType.creditAgricole.sectionIndex {
            // If the selected cell is in the Credit Agricole section, toggle the corresponding showAccountsCABanks boolean value.
            if indexPath.row % 2 == 0 {
                let currentShowAccounts = viewModel.showAccountsCABanks[indexPath.row / 2]
                self.viewModel.showAccountsCABanks[indexPath.row/2] = !currentShowAccounts
            }
        } else if indexPath.section == AccountType.other.sectionIndex {
            // If the selected cell is in the Other section, toggle the corresponding showAccountsOtherBanks boolean value.
            if indexPath.row % 2 == 0 {
                let currentShowAccounts = viewModel.showAccountsOtherBanks[indexPath.row / 2]
                self.viewModel.showAccountsOtherBanks[indexPath.row/2] = !currentShowAccounts
            }
        }
        tableView.reloadData()
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row % 2 == 0 {
            return 60
        } else {
            var showAccounts = false
            if indexPath.section == AccountType.creditAgricole.sectionIndex {
                showAccounts = viewModel.showAccountsCABanks[indexPath.row/2]
                if showAccounts {
                    let accounts = viewModel.getCABanks(at: indexPath.row/2)?.accounts
                    return CGFloat((accounts?.count ?? 0) * 60)
                } else {
                    return 0
                }
            } else if indexPath.section == AccountType.other.sectionIndex {
                showAccounts = viewModel.showAccountsOtherBanks[indexPath.row/2]
                if showAccounts {
                    let accounts = viewModel.getOtherBanks(at: indexPath.row/2)?.accounts
                    return CGFloat((accounts?.count ?? 0) * 60)
                } else {
                    return 0
                }
            } else {
                return 0
            }
        }
    }

         
    // This function returns a table view cell for the specified index path.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Initialize a Bank object and a boolean value to show/hide the accounts.
        var bank = Bank(name: "", isCA: 0, accounts: [])
        var showAccounts = false
        
        // determine the section number and row number of the current cell.
        if indexPath.section == AccountType.creditAgricole.sectionIndex {
            // If the current cell is in the Credit Agricole section, set the bank and showAccounts values accordingly.
            bank = self.viewModel.cABanks[indexPath.row/2]
            showAccounts = viewModel.showAccountsCABanks[indexPath.row/2]
        } else if indexPath.section == AccountType.other.sectionIndex {
            // If the current cell is in the Other section, set the bank and showAccounts values accordingly.
            bank = self.viewModel.otherBanks[indexPath.row/2]
            showAccounts = viewModel.showAccountsOtherBanks[indexPath.row/2]
        }
        
        // determine whether to display the bank name or account details in the cell.
        if indexPath.row % 2 == 0 {
            // If the row number is even, return a BankTableViewCell with the bank information and show/hide accounts value.
            guard let cellBank = tableView.dequeueReusableCell(withIdentifier: "BankTableViewCell", for: indexPath) as? BankTableViewCell else {
                return UITableViewCell()
            }
            cellBank.layoutIfNeeded()
            cellBank.configure(with: bank, showAccounts: showAccounts)
            return cellBank
        } else {
            // If the row number is odd, return a MyAccountsTableViewCell with the list of bank accounts.
            guard let cellAccounts = tableView.dequeueReusableCell(withIdentifier: "MyAccountsTableViewCell", for: indexPath) as? MyAccountsTableViewCell else {
                return UITableViewCell()
            }
            cellAccounts.layoutIfNeeded()
            cellAccounts.configure(with: bank.accounts ?? [])
            // Set the showAccountDetails closure to navigate to the account details screen when an account is selected.
            cellAccounts.showAccountDetails = {[weak self] account in
                self?.viewModel.coordinator?.showAccountDetailsScreen(account: account)
            }
            return cellAccounts
        }
    }
    
    @objc private func refreshData(_ sender: Any) {
        self.viewModel.getBanks()
        self.refreshControl.endRefreshing()
    }

    
    
}
