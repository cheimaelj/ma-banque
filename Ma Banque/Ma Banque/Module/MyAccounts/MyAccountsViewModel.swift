//
//  MyAccountsViewModel.swift
//  Ma Banque
//
//  Created by celj on 25/04/2023.
//

import Foundation

class MyAccountsViewModel {
    
    var coordinator : MyAccountsCoordinator?
    var banks = [Bank]()
    var cABanks = [Bank]()
    var otherBanks = [Bank]()
    var refreshView = {}
    var showError: (String)-> Void = { _ in }
    var showLoader: (Bool)-> Void = { _ in }
    var showAccountsCABanks = [Bool]()
    var showAccountsOtherBanks = [Bool]()
    
    private var banksService = BanksService()
    
    private var isConnectedToNetwork: Bool{
        Current.networkStatus.isConnected
    }
    
    // MARK: - public functions
    
    
    func getCABanks(at index: Int) -> Bank? {
        guard index >= 0 && index < cABanks.count else {
            return nil
        }
        return cABanks[index]
    }
    
    func getOtherBanks(at index: Int) -> Bank? {
        guard index >= 0 && index < otherBanks.count else {
            return nil
        }
        return otherBanks[index]
    }
    
    
    func getBanks() {
        guard isConnectedToNetwork else {
            showError(NetworkError.noInternetConnection.localizedDescription)
            return
        }
        showLoader(true)
        
        banksService.fetchBanks() {[weak self] result in
            self?.showLoader(false)
            switch result {
            case .success(let banks):
                self?.refeshBanks(with: banks)
            case .failure(let error):
                self?.showError((error as? NetworkError )?.localizedDescription ?? "")
            }
        }
    }
    
    public func refeshBanks(with banks: [Bank] = []) {
        self.banks = banks
        filterBanks(with: banks)
        refreshView()
    }
    
    func filterBanks(with banks: [Bank] = []){
        self.cABanks = banks.filter { $0.isCA == AccountType.creditAgricole.isCA }
        showAccountsCABanks = Array(repeating:false, count: self.cABanks.count)
        
        self.otherBanks = banks.filter { $0.isCA == AccountType.other.isCA }
        showAccountsOtherBanks = Array(repeating:false, count: self.otherBanks.count)
    }
    
    
    func showAccountDetailsScreen(account: Account) {
        coordinator?.showAccountDetailsScreen(account: account)
    }
    
}

