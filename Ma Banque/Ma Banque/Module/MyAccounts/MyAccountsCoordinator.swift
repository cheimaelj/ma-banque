//
//  MyAccountsCoordinator.swift
//  Ma Banque
//
//  Created by celj on 25/04/2023.
//

import Foundation
import UIKit

class MyAccountsCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = []
    private var navigationController : UINavigationController
        
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        guard let viewController = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "MyAccountsViewController") as? MyAccountsViewController else {
                return
        }
        let viewModel = MyAccountsViewModel()
        viewController.viewModel = viewModel
        viewController.viewModel.coordinator = self
        self.navigationController.pushViewController(viewController, animated: true)
    }
    
    func showAccountDetailsScreen(account: Account) {
        let myAccountDetailsCoordinator = MyAccountDetailsCoordinator(navigationController: navigationController, account: account)
        myAccountDetailsCoordinator.parentCoordinator = self
        myAccountDetailsCoordinator.start()
        childCoordinators.append(myAccountDetailsCoordinator)
    }
    
   
}
