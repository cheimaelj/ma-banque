//
//  String+Lacalizable.swift
//  Ma Banque
//
//  Created by celj on 26/04/2023.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
