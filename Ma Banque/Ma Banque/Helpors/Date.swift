//
//  Date.swift
//  Ma Banque
//
//  Created by celj on 27/04/2023.
//

import Foundation

func formatUnixTimestamp(_ timestamp: String?) -> String? {
    guard let timestampString = timestamp, let unixTimestamp = Double(timestampString) else {
        return nil
    }
    let date = Date(timeIntervalSince1970: unixTimestamp)
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy"
    return dateFormatter.string(from: date)
}
