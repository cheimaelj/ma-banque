//
//  AppCoordinator.swift
//  Ma Banque
//
//  Created by celj on 25/04/2023.
//

import Foundation
import UIKit

protocol Coordinator : AnyObject {
    var childCoordinators : [Coordinator] { get set }
    func start()
    func childDidFinish(childCoordinator: Coordinator)
}

class AppCoordinator : Coordinator {
    var childCoordinators : [Coordinator] = []
    private var navigationController : UINavigationController!
    private let window : UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        navigationController = UINavigationController()
        
        let tabBarCoordinator = TabBarCoordinator(navigationController: navigationController)
        tabBarCoordinator.start()
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
    
}

extension Coordinator {
    func childDidFinish(childCoordinator: Coordinator) {
        if let index = childCoordinators.firstIndex(where: {
            (coordinator: Coordinator) -> Bool in
            childCoordinator === coordinator
        }) {
            childCoordinators.remove(at: index)
        }
    }
}
