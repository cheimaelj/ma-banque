//
//  Accounts.swift
//  Ma Banque
//
//  Created by celj on 25/04/2023.
//

import Foundation

struct Account : Codable {
    let order : Int?
    let id : String?
    let holder : String?
    let role : Int?
    let contract_number : String?
    let label : String?
    let product_code : String?
    let balance : Double?
    var operations : [Operation]?
    
    enum CodingKeys: String, CodingKey {
        
        case order = "order"
        case id = "id"
        case holder = "holder"
        case role = "role"
        case contract_number = "contract_number"
        case label = "label"
        case product_code = "product_code"
        case balance = "balance"
        case operations = "operations"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        order = try values.decodeIfPresent(Int.self, forKey: .order)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        holder = try values.decodeIfPresent(String.self, forKey: .holder)
        role = try values.decodeIfPresent(Int.self, forKey: .role)
        contract_number = try values.decodeIfPresent(String.self, forKey: .contract_number)
        label = try values.decodeIfPresent(String.self, forKey: .label)
        product_code = try values.decodeIfPresent(String.self, forKey: .product_code)
        balance = try values.decodeIfPresent(Double.self, forKey: .balance)
        operations = try values.decodeIfPresent([Operation].self, forKey: .operations)
    }
    
    init(order : Int?, id : String?, holder : String?, role : Int?, contract_number : String?, label : String?, product_code : String?, balance : Double?, operations : [Operation]?){
        self.order = order
        self.id = id
        self.holder = holder
        self.role = role
        self.contract_number = contract_number
        self.label = label
        self.product_code = product_code
        self.balance = balance
        self.operations = operations
        
    }
    
}
