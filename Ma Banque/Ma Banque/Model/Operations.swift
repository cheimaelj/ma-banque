//
//  Operations.swift
//  Ma Banque
//
//  Created by celj on 25/04/2023.
//

import Foundation

struct Operation : Codable {
    let id : String?
    let title : String?
    let amount : String?
    let category : String?
    let date : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case title = "title"
        case amount = "amount"
        case category = "category"
        case date = "date"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        amount = try values.decodeIfPresent(String.self, forKey: .amount)
        category = try values.decodeIfPresent(String.self, forKey: .category)
        date = try values.decodeIfPresent(String.self, forKey: .date)
    }
    
    init( id : String?, title : String?, amount : String?, category : String?, date : String?){
        self.id = id
        self.title = title
        self.amount = amount
        self.category = category
        self.date = date
    }

}
