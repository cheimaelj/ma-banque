//
//  Banks.swift
//  Ma Banque
//
//  Created by celj on 25/04/2023.
//

import Foundation

struct Bank : Codable {
    let name : String?
    let isCA : Int?
    let accounts : [Account]?

    enum CodingKeys: String, CodingKey {

        case name = "name"
        case isCA = "isCA"
        case accounts = "accounts"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        isCA = try values.decodeIfPresent(Int.self, forKey: .isCA)
        accounts = try values.decodeIfPresent([Account].self, forKey: .accounts)
    }
    
    init(name : String?,  isCA : Int?, accounts : [Account]?){
        self.name = name
        self.isCA = isCA
        self.accounts = accounts
        
    }

}
