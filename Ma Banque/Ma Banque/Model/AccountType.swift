//
//  AccountType.swift
//  Ma Banque
//
//  Created by celj on 26/04/2023.
//

import Foundation

enum AccountType: Error {
    
    case creditAgricole
    case other
   
    static func getAccountType(status: Int) -> AccountType {
        
        switch status {
        case 1:
            return .creditAgricole
        default:
            return .other
        }
    }
    
    var isCA: Int {
        switch self {
        case .creditAgricole:
            return 1
        case .other :
            return 0
        }
    }
    
    var sectionIndex: Int {
        switch self {
        case .creditAgricole:
            return 0
        case .other :
            return 1
        }
    }
    
    var sectionTitle: String {
        switch self {
        case .creditAgricole:
            return "credit_agricole".localized
        case .other :
            return "autres_banques" .localized
        }
    }

}

