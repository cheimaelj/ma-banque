//
//  BanksService.swift
//  Ma Banque
//
//  Created by celj on 25/04/2023.
//

import Foundation

struct BanksService{
    
    func fetchBanks(completion: @escaping (Result<[Bank] , NSError >)->()) {
        
        guard let url = URL(string: "https://cdf-test-mobile-default-rtdb.europe-west1.firebasedatabase.app/banks.json") else { return  }
        
        let networkService = NetworkDataFetcher(session: URLSession.urlSession)
        networkService.performRequest(url: url) { (result) in
            switch result {
            case .success(let data):
                do{
                    let result = try JSONDecoder().decode([Bank].self, from: data)
                    completion(.success(result))
                } catch {
                    //ERROR: if json decoder fails
                    completion(.failure(NSError(domain: "", code: 540)))//invalidResponse
                }
            case .failure(let error):
                //ERROR: if request fails
                completion(.failure(error))
                break
            }
        }
    }
    
    
}
