


# Application Ma Banque

Cette application de banque a été développée par Cheima pour un test technique de poste de développeur iOS chez Crédit Agricole. Elle permet aux utilisateurs d'afficher une liste de tous les comptes bancaires, ainsi que les détails de chaque compte, y compris les opérations effectuées. Elle a été développée en utilisant l'architecture MVVM et des coordinators pour faciliter la navigation entre les différentes vues. Les vues ont été créées en utilisant le Storyboard d'Interface Builder.

## Fonctionnalités

- Affichage de la liste de tous les comptes bancaires disponibles
- Affichage des détails de chaque compte, y compris les opérations effectuées
- Actualisation des données à l'aide de la fonction de tirer pour rafraîchir

## Compatibilité

Cette application est compatible avec les appareils iPhone fonctionnant sous iOS 14 ou ultérieur. Elle n'a pas été conçue pour être utilisée sur un iPad.


## Utilisation

Pour utiliser cette application, vous pouvez cloner ce dépôt et l'ouvrir dans Xcode. Ensuite, vous pouvez exécuter l'application sur votre simulateur ou votre appareil iOS.

## Auteur

Cette application a été développée par Cheima pour un test technique de poste de développeur iOS chez Crédit Agricole. Si vous avez des questions ou des commentaires, n'hésitez pas à me contacter.
